package io.scribe.jpa;

import io.scribe.api.Filter;
import io.scribe.api.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Objects;

public abstract class JpaRepository<K, V> implements Repository<K, V> {

    protected class SQLRenderer implements Filter.Renderer<TypedQuery<V>> {

        @Override
        public TypedQuery<V> render(Filter filter) {

            CriteriaBuilder builder = entityManager.getCriteriaBuilder();

            CriteriaQuery<V> criteria = builder.createQuery(entityType);

            Root<V> entity = criteria.from(entityType);

            criteria.select(entity);

            if(filter != null) {
                if (filter.getQ() != null) {
                    filter.getQ().entrySet().stream()
                            .map(e -> e.getValue().stream()
                                    .map(v -> toPredicate(e.getKey(), v, builder, entity))
                                    .reduce(builder::or)
                                    .orElseThrow(() -> new IllegalArgumentException("No values specified")))
                            .reduce(builder::and)
                            .ifPresent(criteria::where);
                }
            }

            return entityManager.createQuery(criteria);
        }

        @SuppressWarnings("unchecked")
        private Predicate toPredicate(String key, Object value, CriteriaBuilder builder, Root<V> root) {

            int index = key.indexOf(':');

            Expression column = root.get(index > 0 ? key.substring(0, index) : key);

            if(value != null) {
                column = column.as(value.getClass());
            }

            Filter.Comparison comparison = index > 0 ? Filter.Comparison.fromString(key.substring(index + 1)) : Filter.Comparison.EQUAL;

            switch(comparison) {

                case EQUAL:
                    return value == null ? builder.isNull(column) : builder.equal(column, value);

                case LESS_THAN:
                    return builder.lessThan(column, (Comparable) value);

                case LESS_THAN_EQUAL:
                    return builder.lessThanOrEqualTo(column, (Comparable) value);

                case GREATER_THAN:
                    return builder.greaterThan(column, (Comparable) value);

                case GREATER_THAN_EQUAL:
                    return builder.greaterThanOrEqualTo(column, (Comparable) value);

                case NOT_EQUAL:
                    return value == null ? builder.isNotNull(column) : builder.notEqual(column, value);
            }

            return null;
        }

    }

    private final Class<V> entityType;

    private final EntityManager entityManager;

    private final Filter.Renderer<TypedQuery<V>> filterRenderer;

    protected JpaRepository(EntityManager entityManager, Class<V> entityType) {
        this.entityManager = entityManager;
        this.entityType = entityType;
        this.filterRenderer = new SQLRenderer();
    }

    public V persist(V entity) {
        entityManager.persist(entity);
        return entity;
    }

    public void update(V entity) {
        entityManager.merge(entity);
    }

    public void delete(V entity) {
        entityManager.remove(entity);
    }

    public V find(K id) {
        return entityManager.find(entityType, id);
    }

    public List<V> findAll(Filter filter) {
        return filterRenderer.render(filter).getResultList();
    }

}
