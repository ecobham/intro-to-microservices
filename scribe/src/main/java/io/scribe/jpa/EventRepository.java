package io.scribe.jpa;

import io.scribe.model.Event;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.UUID;

public class EventRepository extends JpaRepository<UUID, Event> {

    @Inject
    public EventRepository(EntityManager entityManager) {
        super(entityManager, Event.class);
    }

}
