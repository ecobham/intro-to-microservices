package io.scribe.jpa;

import io.scribe.api.Filter;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Objects;

public class SQLRenderer<T> implements Filter.Renderer<TypedQuery<T>> {

    private final EntityManager entityManager;

    private final Class<T> entityType;

    public SQLRenderer(EntityManager entityManager, Class<T> entityType) {
        this.entityManager = Objects.requireNonNull(entityManager, "Non-null entity manager required");
        this.entityType = Objects.requireNonNull(entityType, "Non-null entity type required");
    }

    @Override
    public TypedQuery<T> render(Filter filter) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteria = builder.createQuery(entityType);

        Root<T> entity = criteria.from(entityType);

        criteria.select(entity);

        if(filter != null) {
            if (filter.getQ() != null) {
                filter.getQ().entrySet().stream()
                    .map(e -> e.getValue().stream()
                        .map(v -> toPredicate(e.getKey(), v, builder, entity))
                        .reduce(builder::or)
                        .orElseThrow(() -> new IllegalArgumentException("No values specified")))
                    .reduce(builder::and)
                    .ifPresent(criteria::where);
            }
        }

        return entityManager.createQuery(criteria);
    }

    @SuppressWarnings("unchecked")
    private Predicate toPredicate(String key, Object value, CriteriaBuilder builder, Root<T> root) {

        int index = key.indexOf(':');

        Expression column = root.get(index > 0 ? key.substring(0, index) : key);

        if(value != null) {
            column = column.as(value.getClass());
        }

        Filter.Comparison comparison = index > 0 ? Filter.Comparison.fromString(key.substring(index + 1)) : Filter.Comparison.EQUAL;

        switch(comparison) {

            case EQUAL:
                return value == null ? builder.isNull(column) : builder.equal(column, value);

            case LESS_THAN:
                return builder.lessThan(column, (Comparable) value);

            case LESS_THAN_EQUAL:
                return builder.lessThanOrEqualTo(column, (Comparable) value);

            case GREATER_THAN:
                return builder.greaterThan(column, (Comparable) value);

            case GREATER_THAN_EQUAL:
                return builder.greaterThanOrEqualTo(column, (Comparable) value);

            case NOT_EQUAL:
                return value == null ? builder.isNotNull(column) : builder.notEqual(column, value);
        }

        return null;
    }

}
