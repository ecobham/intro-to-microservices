package io.scribe.api;


import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.spring.tx.annotation.Transactional;
import io.scribe.Application;
import io.scribe.jpa.EventRepository;
import io.scribe.model.Event;

import javax.annotation.Nullable;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.micronaut.security.rules.SecurityRule.IS_AUTHENTICATED;

@Controller("/event")
@Secured(IS_AUTHENTICATED)
public class EventResource {

    @Inject
    private EventRepository repository;

    /**
     * Retrieves an event by id
     * @param id
     * @return
     */
    @Get("/{id}")
    @RolesAllowed(Application.Roles.EVENT_CONSUMER)
    @Transactional
    public Event find(UUID id) {
        return repository.find(id);
    }

    /**
     * Searches for paginated events with an optional filter
     * @param filter
     * @return a list of events matching the provided filter if present
     */
    @Post("/search")
    @RolesAllowed(Application.Roles.EVENT_CONSUMER)
    @Transactional
    public List<Event> findAll(@Nullable @Body Filter filter) {
        return repository.findAll(filter);
    }

    /**
     * Creates a new event
     * @param event
     * @return the created event
     */
    @Post
    @RolesAllowed(Application.Roles.EVENT_PUBLISHER)
    @Transactional
    public Event persist(@Body Event event) {
        return repository.persist(event);
    }

}
