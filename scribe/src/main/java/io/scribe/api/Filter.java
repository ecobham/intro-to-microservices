package io.scribe.api;

import lombok.Data;

import java.util.*;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

@Data
public class Filter {

    @FunctionalInterface
    public interface Renderer<T> {

        T render(Filter filter);

    }

    public enum Comparison {

        EQUAL("eq", "="),
        LESS_THAN("lt", "<"),
        LESS_THAN_EQUAL("lte", "<="),
        GREATER_THAN("gt", ">"),
        GREATER_THAN_EQUAL("gte", ">="),
        NOT_EQUAL("ne", "!=");

        private Comparison(String... values) {
            this.values = stream(values).collect(toSet());
        }

        private final Set<String> values;

        public static Comparison fromString(String value) {
            for(Comparison comparison : Comparison.values()) {
                if(comparison.values.contains(value)) {
                    return comparison;
                }
            }
            throw new IllegalArgumentException(String.format("No such value '%s'", value));
        }

    }

    private Integer limit;

    private Integer offset;

    private String orderBy;

    private Map<String, List<Object>> q;

    public Map<String, List<Object>> getQ() {
        return q != null ? q : Collections.emptyMap();
    }

}
