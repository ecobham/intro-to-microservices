package io.scribe.api;

import java.util.List;

public interface Repository<K, V> {

    public V persist(V entity);

    public void update(V entity);

    public void delete(V entity);

    public V find(K id);

    public List<V> findAll(Filter filter);

}
