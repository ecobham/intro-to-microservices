package io.scribe.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import io.scribe.jpa.JsonToMapConverter;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Table
@Data
public class Event {

    @NotBlank
    @Column(columnDefinition = "varchar(36)", length = 64, nullable = false)
    private String action;

    @NotBlank
    @Column(columnDefinition = "varchar(36)", length = 64, nullable = false)
    private String actor;

    @CreationTimestamp
    @Column(columnDefinition = "timestamp", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "varchar(36)", length = 36, nullable = false, updatable = false)
    private UUID id;

    @NotBlank
    @Column(columnDefinition = "varchar(36)", length = 64, nullable = false)
    private String target;

}
